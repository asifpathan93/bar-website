var winWidth,
    winHeight, mobile_breakbpoint = 767;

function get_height_width() {
    winWidth = $(window).width(),
        winHeight = $(window).height();
}

function set_height_width() {
    if ($('body').height() < winHeight) {
        $('.wh').outerHeight(winHeight);
    }
    $('.wh-min').css('min-height', winHeight);
    $('.ww').outerWidth(winWidth);
}

function set_background() {
    $('.set-bg').each(function() {
        var align = $(this).attr('data-align');
        if (typeof $(this).attr('data-mob-img') === 'undefined') {
            $(this).css({
                'background': 'url(' + $(this).attr('data-img') + ')',
                'background-size': 'cover',
                'background-position': 'center center',//align,
                'background-repeat': 'no-repeat'
            });
        } else {
            if (winWidth > mobile_breakbpoint) {
                if (typeof $(this).attr('data-img') != 'undefined') {
                    $(this).css({
                        'background': 'url(' + $(this).attr('data-img') + ')',
                        'background-size': 'cover'
                    });
                }
            } else {
                $(this).css({
                    'background': 'url(' + $(this).attr('data-mob-img') + ')',
                    'background-size': 'cover'
                });
            }
        }
    });
}

function custom_select() {
    $('.select').customSelect();
}

function langChange(){
    $('.lng-parent select').next().find('.customSelectInner').html($('option:selected', $('.lng-parent select')).attr('value'));
    $('.lng-parent select').change(function(){
        console.log($('option:selected', this).attr('value'));
        $(this).next().find('.customSelectInner').html($('option:selected', this).attr('value'));
    });
}
function search_box() {
    $('.bs-search-box').each(function() {

        element = $(this);
        element.find('.search-input').on('focus', function() {
            element.find('.search-list-parent').fadeIn();
        });
        element.find('.search-input').on('focusout', function() {
            element.find('.search-list-parent').fadeOut();
        });
    });
}

function rate() {
    $(".my-rating").find(".icon").hover(function() {
        $(this).prevAll().andSelf().addClass('icon-rating-fill').removeClass('icon-rating');
        $(this).nextAll().removeClass('icon-rating-fill').addClass('icon-rating');
    }, function() {
        $(this).siblings().andSelf().addClass('icon-rating').removeClass('icon-rating-fill');
    });
    $(".my-rating .icon").click(function() {
        //$(this).siblings().off("hover");
        $(this).siblings().andSelf().unbind('mouseenter mouseleave');
        $(this).prevAll().andSelf().addClass('icon-rating-fill').removeClass('icon-rating');
        $(this).nextAll().removeClass('icon-rating-fill').addClass('icon-rating');
        $(this).parent().attr('data-star', '' + ($(this).index() + 1)).addClass('clicked');
    });
}
//check all 
function checkAll() {
    $('.all-check').change(function() {
        if ($(this).prop("checked") == false) {
            $(this).closest('.checkbox-parent').siblings().find('input').prop('checked', false);
        } else {
            $(this).closest('.checkbox-parent').siblings().find('input').prop('checked', true);
        }
    });
}

function back_swiper() {
    var element = $('.back-swiper');
    var swiper1 = new Swiper('.back-swiper', {

        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        fadeEffect: {
            crossFade: true
        },
        effect: 'fade',

    });

}

function swiper_call(element, slidesperview, rsslidesperview) {

    var swiper = new Swiper(element, {
        slidesPerView: slidesperview,
        spaceBetween: 30,
        pagination: {
            el: $(element).next('.swiper-pagination'),
            clickable: true,
        },
        breakpoints: {
            767: {
                slidesPerView: rsslidesperview,
                spaceBetween: 10,
            }
        }
    });

    if ($(element + " .swiper-slide").length == 1) {
        $(element + ' .swiper-wrapper').addClass("disabled");
        $(element).closest('.lyt-wb').find('.swiper-pagination').addClass("disabled");
    }
}

function swiper_for_mobile(element, rsslidesperview) {

    var swiper = new Swiper(element, {
        slidesPerView: 2,
        spaceBetween: 30,
        pagination: {
            el: $(element).next('.swiper-pagination'),
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1199: {
                slidesPerView: rsslidesperview,
                spaceBetween: 30,
            },
            992: {
                slidesPerView: rsslidesperview,
                spaceBetween: 10,
            }
        }
    });

    if ($(element + " .swiper-slide").length == 1) {
        $(element + ' .swiper-wrapper').addClass("disabled");
        $(element).closest('.lyt-wb').find('.swiper-pagination').addClass("disabled");
    }
}

function noUiSliderline() {
    if ($('#distance-slider').length > 0) {
        var slider = document.getElementById('distance-slider');
        noUiSlider.create(slider, {
            start: [20, 80],
            connect: true,
            range: {
                'min': 0,
                'max': 100
            }
        });
    }

}

function custom_scrollbar() {
    if ($(".scroll-box").length > 0) {
        $(".scroll-box").mCustomScrollbar()
    }
}

function datepicker() {
    $('.datepicker').datepicker({
        format: 'mm-dd-yyyy',
        todayHighlight: true,
        orientation: 'auto top',
        autoclose: true,
        container: '.bs-form',
        startDate: new Date()
    });
}

var map;

function initMap() {
    if ($('#map').length >= 1) {

        map = new google.maps.Map(
            document.getElementById('map'), {
                center: new google.maps.LatLng(-33.91722, 151.23064),
                zoom: 16
            });

        var iconBase =
            'https://www.creativewebo.com/clients/bar-website/assets/images/';

        var icons = {
            icon1: {
                icon: iconBase + 'map-pointer.png'
            }
        };

        var features = [{
                position: new google.maps.LatLng(-33.91722, 151.23064),
                type: 'icon1'
            },
            // {
            //     position: new google.maps.LatLng(-33.91539, 151.22820),
            //     type: 'icon1'
            // }, 
            // {
            //     position: new google.maps.LatLng(-33.91747, 151.22912),
            //     type: 'icon1'
            // }

        ];

        // Create markers.
        for (var i = 0; i < features.length; i++) {
            var marker = new google.maps.Marker({
                position: features[i].position,
                icon: icons[features[i].type].icon,
                map: map
            });
        };
    }

}


function filter_formobile() {
    $('.filter-click').click(function() {
        $('.lp-left-info ').addClass('active');
    });
    $('.btn-done').click(function(e) {
        e.preventDefault();
        $('.lp-left-info ').removeClass('active');
    });
}

function material_input() {
    var input = $('.material .form-control');
    input.focus(function() {
        $(this).parent().addClass('focus');
    });
    input.focusout(function() {
        if ($(this).parent().hasClass('readonly') == false) {
            if ($(this).val() == '') {
                $(this).parent().removeClass('focus');
            }
        }
    });
}

function edit_input() {
    $('.inputedit').click(function() {
        $(this).hide();
        $(this).next().removeAttr('readonly');
    });
}




function images_popup() {
    var groups = {};
    $('.galleryItem').each(function() {
        var id = parseInt($(this).attr('data-group'), 10);

        if (!groups[id]) {
            groups[id] = [];
        }

        groups[id].push(this);
    });


    $.each(groups, function() {

        $(this).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: false,
            gallery: { enabled: true }
        })

    });
}

function read_more() {

    $('.read-more').click(function() {
        if ($(this).closest('.tab-para').hasClass('open') == true) {
            $(this).closest('.tab-para').find('.more').hide();
            $(this).closest('.tab-para').find('.dots').hide();
            $(this).html('read more');
            $(this).closest('.tab-para').removeClass('open');

        } else {
            $(this).closest('.tab-para').find('.more').show();
            $(this).closest('.tab-para').find('.dots').hide();
            $(this).closest('.tab-para').addClass('open');
            $(this).html('read less');
        }
    });
}

function tab_scroll() {
    $('.nav-tabs').each(function() {
        var tab_width = 0,
            tab_parent = $(this);
        $(this).find('li').each(function() {
            tab_width += $(this).innerWidth() + 10;
        });
        tab_parent.width(tab_width + 'px');
    });
}

function validation() {
    $("#login-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());


        },
        submitHandler: function(form) {
            return false;
        }
    });
}

function intinput() {
    $("#telephone").intlTelInput({
        separateDialCode: true
    });

}

function randomToast_success() {
    var priority = 'success';
    var title = 'Success';
    var message = 'It worked!';

    $.toaster({ priority: priority, title: title, message: message });
}

function randomToast_danger() {
    var priority = 'danger';
    var title = 'danger';
    var message = 'It not worked!';

    $.toaster({ priority: priority, title: title, message: message });
}

function toast_msg() {
    $('#btnstart-success').click(function() {
        randomToast_success();
    });
    $('#btnstart-warning').click(function() {
        randomToast_danger();
    });
}

function btn_loader() {
    $('.loader').click(function() {
        $(this).hide();
        $('.loader-img1').show();

    });
}

function fun_overlay() {
    $('.btn-overlay').click(function() {

        $('.ctm-overlay').show();

    });
}

function social_box() {
    $(document).click(function() {
        $(".share-box .share-popup").hide()
    });
    $(".share-box").click(function(e) {
        e.stopPropagation();
        //return false;     
        $(".share-box .share-popup").toggle();
    });
}




$(function() {
    rate();
    checkAll();
    get_height_width();
    set_height_width();
    set_background();
    custom_select();
    //swiper_call();
    swiper_call('#resent-slider', 3, 1);
    swiper_call('#featured-slider', 4, 1);
    swiper_call('#popular-slider', 4, 1);
    search_box();
    datepicker();
    noUiSliderline();
    filter_formobile();
    material_input();
    images_popup();
    read_more();
    edit_input();
    validation();
    intinput();
    initMap();
    toast_msg();
    btn_loader();
    fun_overlay();
    back_swiper();
    social_box();
    langChange();
    if ($(window).width() > 1199) {
        custom_scrollbar();
    }
    // if ($(window).width() < 992) {
    //     swiper_for_mobile('.slider-for-mobile', 2, 2);
    // }
    if ($(window).width() < 1199) {
        swiper_for_mobile('.slider-for-mobile', 2);
    }
    if ($(window).width() < 992) {
        swiper_for_mobile('.slider-for-mobile', 1);
    }
});

$(window).load(function() {
    tab_scroll();
});